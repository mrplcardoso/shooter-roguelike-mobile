# README #

### What is this repository for? ###

Repository dedicated to keeping the study and implementation 
of a shooter-roguelike beta game for mobile platform (Android).

### How do I get set up? ###

* Used softwares:

	Unity 2020.2 for Windows

	Visual Studio Community 2019

* Used packages:

	https://assetstore.unity.com/packages/2d/environments/2d-fantasy-forest-tileset-19553

	https://opengameart.org/content/armored-soldiers

	https://opengameart.org/content/2d-tanks-top-down

### Support Material ###

* 2D Movement and Jumping:
	
	https://www.youtube.com/watch?v=B2K1417L_HQ&ab_channel=ProgramandoGames

	https://www.youtube.com/watch?v=whzomFgjT50&ab_channel=Brackeys

	https://www.youtube.com/watch?v=ptvK4Fp5vRY&ab_channel=CodeMonkey

* Screen Adjustment:
	
	https://producaodejogos.com/camera-ortografica-no-unity/

* Health Bar:

	https://www.youtube.com/watch?v=BLfNP4Sc_iA&ab_channel=Brackeys

* Projectile:
	
	https://www.youtube.com/watch?v=LNLVOjbrQj4&ab_channel=Brackeys

* Tilemap:

	https://www.youtube.com/watch?v=ryISV_nH8qw


* Procedural Generation:

	https://www.youtube.com/watch?v=1-HIA6-LBJc

	https://www.youtube.com/watch?v=qAf9axsyijY
	
	https://www.youtube.com/watch?v=hk6cUanSfXQ

* RNG:

	https://blog.redbluegames.com/generating-predictable-random-numbers-in-unity-c97b7c4895ec

	https://blogs.unity3d.com/pt/2015/01/07/a-primer-on-repeatable-random-numbers/


### Who do I talk to? ###

* Repository owner or administrator.

In case of use in other projects, please do not forget 
to give the due credits / source.

# Leia-me #

### Para que serve este repositório? ###

Repositório dedicado a guardar o estudo e implementação 
de uma versão beta de um jogo shooter-roguelike para plataformas móveis (Android).

### Como faço para configurar? ###

* Programas usados:

	Unity 2020.2 para Windows

	Visual Studio Community 2019

* Pacotes usados:

	https://assetstore.unity.com/packages/2d/environments/2d-fantasy-forest-tileset-19553

	https://opengameart.org/content/armored-soldiers

	https://opengameart.org/content/2d-tanks-top-down

### Support Material ###

* Movimento 2D e Pulo:
	
	https://www.youtube.com/watch?v=B2K1417L_HQ&ab_channel=ProgramandoGames

	https://www.youtube.com/watch?v=whzomFgjT50&ab_channel=Brackeys

	https://www.youtube.com/watch?v=ptvK4Fp5vRY&ab_channel=CodeMonkey

* Ajuste de tela:
	
	https://producaodejogos.com/camera-ortografica-no-unity/

* Barra de vida:

	https://www.youtube.com/watch?v=BLfNP4Sc_iA&ab_channel=Brackeys

* Projétil:
	
	https://www.youtube.com/watch?v=LNLVOjbrQj4&ab_channel=Brackeys

* Tilemap:

	https://www.youtube.com/watch?v=ryISV_nH8qw

* Geração Procedural

	https://www.youtube.com/watch?v=1-HIA6-LBJc

	https://www.youtube.com/watch?v=qAf9axsyijY
	
	https://www.youtube.com/watch?v=hk6cUanSfXQ

* RNG:

	https://blog.redbluegames.com/generating-predictable-random-numbers-in-unity-c97b7c4895ec

	https://blogs.unity3d.com/pt/2015/01/07/a-primer-on-repeatable-random-numbers/


### Com quem eu falo? ###

* Proprietário ou administrador do repositório.

Em caso de uso em outros projetos, por favor não se esqueça
de dar os devidos créditos/fonte.