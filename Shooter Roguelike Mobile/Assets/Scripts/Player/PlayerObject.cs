using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : MonoBehaviour
{
	public Rigidbody2D body { get; private set; }
	//C�mera presente na cena
	public Camera mainCamera { get; private set; }
	//Refer�ncia para a mira, instancia e sobreescreve o valor da variavel
	public CrossHair crossHair;

	public delegate void ReactionToPlayer(EventArgs e);
	List<ReactionToPlayer> reactions;

	//M�todo de contole da mira
	public ControlType type;
	//Propriedade que retorna a posi��o do mouse/touch convertida para o mundo
	public Vector2 clickPosition
	{
		get
		{
			if (HandleControl.type == ControlType.Mouse)
			{ return mainCamera.ScreenToWorldPoint(Input.mousePosition); }
			else { return mainCamera.ScreenToWorldPoint(Input.GetTouch(0).position); }
		}
	}

	public int currentLife;
	public readonly int maxlife = 5;


	private void Awake()
	{
		mainCamera = Camera.main;
		crossHair = Instantiate(crossHair);
		PublicData.playerReference = this;
		body = GetComponent<Rigidbody2D>();
		reactions = new List<ReactionToPlayer>();
		HandleControl.type = type;
	}

	public void AddPlayerObserver(ReactionToPlayer observer)
	{
		if (!reactions.Contains(observer))
		{ reactions.Add(observer); }
	}

	public void RemovePlayerObserver(ReactionToPlayer observer)
	{
		if (reactions.Contains(observer))
		{ reactions.Remove(observer); }
	}

	public void RunReactions(EventArgs e)
	{
		for(int i = 0; i < reactions.Count; ++i)
		{ reactions[i](e); }
	}
}
