using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
	PlayerObject playerObject;

	public PoolableProjectile prefab;
	ObjectPooler<PoolableProjectile> firePool;
	CrossHair crossHair { get { return playerObject.crossHair; } }

	public LayerMask enemyLayer;
	Vector2 clickPosition { get { return playerObject.clickPosition; } }
	Vector2 cachedPosition;


	private void Awake()
	{
		playerObject = GetComponent<PlayerObject>();
	}

	private void Start()
	{
		firePool = new ObjectPooler<PoolableProjectile>(prefab, 10, 50);
	}

	private void Update()
	{
		Shoot();
	}

	void Shoot()
	{
		if (Input.GetMouseButtonDown(0) || Input.touchCount > 0)
		{
			//Cria uma �rea de colis�o em formato de circulo com centro no "clickPosition"
			//Retorna o collider do primeiro objeto que colidiu com essa �rea.
			Collider2D col = Physics2D.OverlapCircle(clickPosition, 1f, enemyLayer);
			if (col == null) return;

			playerObject.RunReactions(EventArgs.Empty);
			crossHair.SetOverTarget(clickPosition);
			TriggerProjectile();
		}
	}

	void TriggerProjectile()
	{
		cachedPosition = clickPosition;
		PoolableProjectile p = firePool.GetObject();
		Vector3 v = transform.position;
		v.z = 0;
		p.transform.position = v;
		v = (cachedPosition - (Vector2)v).normalized;
		playerObject.body.rotation = (Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg) - 90f;

		p.gameObject.layer = LayerMask.NameToLayer("PlayerLayer");
		p.GetComponent<ProjectileObject>().ownerTag =
			gameObject.tag;
		p.Activate(2f);
		p.GetComponent<ProjectileObject>().SetMove(
			v, 4f);
	}
}
