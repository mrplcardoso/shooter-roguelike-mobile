using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{
	PlayerObject playerObject;
	int currentLife
	{
		get { return playerObject.currentLife; }
		set { playerObject.currentLife = Mathf.Clamp(value, 0, playerObject.maxlife); }
	}

	//Ambos itens abaixo s�o definidos pelo Inspector
	public GameObject lifeContainer;
	public Image lifeIcon;

	readonly Color lifeColor = new Color(1, 1, 1, 1);
	readonly Color lostLifeColor = new Color(0.5f, 0.5f, 0.5f, 1);

	List<Image> lifes;

	private void Awake()
	{
		playerObject = GetComponent<PlayerObject>();
		lifes = new List<Image>();
	}

	private void Start()
	{
		currentLife = playerObject.maxlife;
		for (int i = 0; i < playerObject.maxlife; ++i)
		{
			Vector2 p = new Vector2(lifeIcon.rectTransform.rect.width * 1.5f + (i * lifeIcon.rectTransform.rect.width),
				lifeContainer.transform.position.y);
			Image l = Instantiate(lifeIcon, p, Quaternion.identity, lifeContainer.transform);
			l.color = lifeColor;
			lifes.Add(l);
		}
	}

	void AddLife()
	{
		lifes[currentLife - 1].color = lifeColor;
		currentLife++;
	}
	void SubtractLife()
	{
		if (currentLife - 1 < 0)
		{ return; }
		lifes[currentLife - 1].color = lostLifeColor;
		currentLife--;
		if (currentLife == 0)
		{ /*Morte*/}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Projectile"))
		{
			SubtractLife();
			PoolableProjectile p = collision.gameObject.GetComponent<PoolableProjectile>();
			p.DeActivate();
		}
		if (collision.gameObject.CompareTag("Enemy"))
		{
			SubtractLife();
		}
	}
}
