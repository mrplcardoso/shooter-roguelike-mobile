using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableProjectile : MonoBehaviour, IPoolableObject
{
	int id = -1;
	public int poolIndex 
	{ get { return id; } set { if (id < 0) { id = value; } } }

	public bool activeInScene
	{ get { return gameObject.activeSelf; } }

	//Tempo de atividade do projetil em cena
	float currentTime;
	public float leftDuration
	{ get { return currentTime; } }

	public bool stopTimer 
	{ 
		get { if (currentTime < 0) { return true; } return false; }
		set { if (value) { currentTime = -1; } }
	}

	public void Activate(float duration)
	{
		gameObject.SetActive(true);
		currentTime = duration;
		StartCoroutine(Timer());
	}

	public void DeActivate()
	{
		currentTime = -1;
		gameObject.SetActive(false);
	}

	/*Corotina que inicia toda vez que o objeto � ativado em cena
	 Funciona como um temporizador para a vida do objeto projetil
	 ou seja, a cada frame, ela decrementa a contagem de tempo
	 do objeto em cena. Quando esse tempo chrga a zero, o objeto
	 � desativado da cena
	*/
	IEnumerator Timer()
	{
		while(currentTime > 0)
		{
			currentTime -= Time.deltaTime;
			yield return null;
		}
		gameObject.SetActive(false);
	}
}
