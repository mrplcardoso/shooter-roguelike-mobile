using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
	PlayerObject playerObject;
	Rigidbody2D body { get { return playerObject.body; } }
	public LayerMask walkMask;
	Vector2 clickPosition { get { return playerObject.clickPosition; } }
	Vector2 direction;
	Vector2 rotationDirection;
	float speed;
	
	private void Awake()
	{
		playerObject = GetComponent<PlayerObject>();
		speed = 3f;
	}

	private void Update()
	{
		//Retorna verdadeiro enquanto o bot�o esquerdo estiver pressionado
		if(Input.GetMouseButton(0) || Input.touchCount > 0)
		{
			//Cria uma �rea de colis�o em formato de circulo com centro no "clickPosition"
			//Retorna o collider do primeiro objeto que colidiu com essa �rea.
			Collider2D col = Physics2D.OverlapCircle(clickPosition, 1f, walkMask);
			if(col == null)
			{
				//Dire��o que move objeto A � objeto B -> posi��o do B - posi��o do A
				rotationDirection = direction = (clickPosition - (Vector2)transform.position).normalized;
				playerObject.RunReactions(EventArgs.Empty);
			}
		}
		else { direction = Vector2.zero; }
	}

	private void FixedUpdate()
	{
		body.MovePosition(body.position + direction * speed * Time.fixedDeltaTime);
		body.rotation = (Mathf.Atan2(rotationDirection.y, rotationDirection.x) * Mathf.Rad2Deg) - 90f;
	}
}
