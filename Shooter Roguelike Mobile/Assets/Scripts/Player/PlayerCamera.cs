using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
	PlayerObject playerObject;
	Transform sceneCamera { get { return playerObject.mainCamera.transform; } }
	Vector3 next;
	float cameraSpeed;

	private void Awake()
	{
		playerObject = GetComponent<PlayerObject>();
		cameraSpeed = 3 * 0.5f;
	}

	private void FixedUpdate()
	{
		MoveCamera();
	}

	void MoveCamera()
	{
		next = Vector3.Lerp(sceneCamera.position,
									playerObject.body.position, cameraSpeed * Time.fixedDeltaTime);

		sceneCamera.position = new Vector3(next.x, next.y, 
			sceneCamera.position.z);
	}
}
