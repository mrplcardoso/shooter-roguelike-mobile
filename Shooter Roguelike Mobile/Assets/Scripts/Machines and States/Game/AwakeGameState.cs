using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeGameState : GameState
{
	bool genComplete, transitionComplete, stateEntered;

	private void Awake()
	{
		genComplete = transitionComplete = stateEntered = false;
		this.AddObserver(OnGenerationCompleted, Notifications.RoomGenerationCompleted);
	}

	public void OnGenerationCompleted(object sneder, EventArgs e)
	{
		genComplete = true;
		this.PostNotification(Notifications.TriggerTransitionScreen, 
			new MessageArgs("open"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public override IEnumerator OnEnterIntervaled()
	{
		gameMachine.runningState = this;
		stateEntered = true;

		yield return new WaitWhile(() => !stateEntered &&
		!genComplete && !transitionComplete);

		gameMachine.ChangeStateIntervaled<StartGameState>();
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		transitionComplete = true;
	}
}
