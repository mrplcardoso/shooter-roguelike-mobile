using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMachine : AbstractStateMachine
{
	public AbstractState runningState;

	protected override void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		ChangeStateImmediate<AwakeGameState>();
	}
}
