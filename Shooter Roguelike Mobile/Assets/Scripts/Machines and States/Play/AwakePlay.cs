﻿using System;
using System.Collections;
using UnityEngine;

public class AwakePlay : PlayState
{
	public override void OnEnter()
	{
		playMachine.runningState = this;
		PublicData.currentPlayerName = "";
		PublicData.currentSeed = -1;
		this.PostNotification(Notifications.TriggerTransitionScreen, new MessageArgs("open"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		playMachine.ChangeStateIntervaled<MainPlay>();
	}
}