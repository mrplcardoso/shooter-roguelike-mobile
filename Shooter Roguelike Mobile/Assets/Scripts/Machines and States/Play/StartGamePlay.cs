﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGamePlay : PlayState
{
	public override void OnEnter()
	{
		playMachine.runningState = this;
		this.PostNotification(Notifications.TriggerTransitionScreen, new MessageArgs("close"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		if (PublicData.currentSeed < 0)
		{ PublicData.mapRandom = new RandomStream(); }
		else
		{ PublicData.mapRandom = new RandomStream(PublicData.currentSeed); }
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		SceneManager.LoadScene("Game");
	}
}