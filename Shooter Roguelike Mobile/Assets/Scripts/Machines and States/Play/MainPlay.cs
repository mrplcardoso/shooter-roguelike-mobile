﻿using System;
using System.Collections;
using UnityEngine;

public class MainPlay : PlayState
{
	[SerializeField]
	int validationCounter;

	private void Awake()
	{
		this.AddObserver(OnValidName, Notifications.ValidNamePlay);
		this.AddObserver(OnValidSeed, Notifications.ValidSeedPlay);
		this.AddObserver(OnBackButton, Notifications.BackButtonPressed);
	}

	public override void OnEnter()
	{
		playMachine.runningState = this;
		this.PostNotification(Notifications.EnterMainPlayState);
	}

	public override void OnExit()
	{
		this.PostNotification(Notifications.ExitMainPlayState);
	}

	public void OnValidName(object sender, EventArgs e)
	{
		bool b = (bool)((MessageArgs)e).message;
		if (b)
		{ validationCounter++; }
		else
		{ validationCounter = 0; }
		if (validationCounter > 1)
		{ playMachine.ChangeStateIntervaled<StartGamePlay>(); }
	}

	public void OnValidSeed(object sender, EventArgs e)
	{
		bool b = (bool)((MessageArgs)e).message;
		if (b)
		{ validationCounter++; }
		else
		{ validationCounter = 0; }
		if (validationCounter > 1)
		{ playMachine.ChangeStateIntervaled<StartGamePlay>(); }
	}

	public void OnBackButton(object sender, EventArgs e)
	{
		playMachine.ChangeStateIntervaled<BackPlay>();
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.ValidNamePlay, OnValidName);
		this.RemoveObserver(Notifications.ValidSeedPlay, OnValidSeed);
		this.RemoveObserver(Notifications.BackButtonPressed, OnBackButton);
	}
}