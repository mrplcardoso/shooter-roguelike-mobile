using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayState : AbstractState
{
	public PlayMachine playMachine
	{ get { return (PlayMachine)stateMachine; } }
}
