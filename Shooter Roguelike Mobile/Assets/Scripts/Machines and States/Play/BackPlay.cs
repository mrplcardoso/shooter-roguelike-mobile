﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackPlay : PlayState
{
	public override void OnEnter()
	{
		playMachine.runningState = this;
		this.PostNotification(Notifications.TriggerTransitionScreen, new MessageArgs("close"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		SceneManager.LoadScene("Title");
	}
}