using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMachine : AbstractStateMachine
{
	public AbstractState runningState;

	protected override void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		ChangeStateIntervaled<AwakePlay>(1);
	}
}
