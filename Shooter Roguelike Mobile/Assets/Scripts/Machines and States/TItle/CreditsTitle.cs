using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsTitle : TitleState
{
	private void Awake()
	{
		this.AddObserver(OnBackPressed, Notifications.ExitCreditsButtonPressed);
	}

	public override void OnEnter()
	{
		titleMachine.runningState = this;
		this.PostNotification(Notifications.EnterCreditsState);
	}

	public override void OnExit()
	{
		this.PostNotification(Notifications.ExitCreditsState);
	}

	public void OnBackPressed(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<MainTitle>();
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.ExitCreditsButtonPressed, OnBackPressed);
	}
}
