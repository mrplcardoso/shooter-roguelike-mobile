using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingTitle : TitleState
{
	private void Awake()
	{
		this.AddObserver(OnBackPressed, Notifications.ExitRankingButtonPressed);
	}

	public override void OnEnter()
	{
		titleMachine.runningState = this;
		this.PostNotification(Notifications.EnterRankingState);
	}

	public override void OnExit()
	{
		this.PostNotification(Notifications.ExitRankingState);
	}

	public void OnBackPressed(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<MainTitle>();
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.ExitRankingButtonPressed, OnBackPressed);
	}
}
