using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainTitle : TitleState
{
	private void Awake()
	{
		this.AddObserver(OnPlayButton, Notifications.PlayButtonPressed);
		this.AddObserver(OnRankingButton, Notifications.RankingButtonPressed);
		this.AddObserver(OnCreditsButton, Notifications.CreditsButtonPressed);
		this.AddObserver(OnExitButton, Notifications.ExitButtonPressed);
	}

	public override void OnEnter()
	{
		titleMachine.runningState = this;
		this.PostNotification(Notifications.EnterMainState);
	}

	public override void OnExit()
	{
		this.PostNotification(Notifications.ExitMainState);
	}

	public void OnPlayButton(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<PlayTitle>();
	}

	public void OnRankingButton(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<RankingTitle>();
	}

	public void OnCreditsButton(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<CreditsTitle>();
	}

	public void OnExitButton(object sender, EventArgs e)
	{
		titleMachine.ChangeStateIntervaled<ExitTitle>();
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.PlayButtonPressed, OnPlayButton);
		this.RemoveObserver(Notifications.RankingButtonPressed, OnRankingButton);
		this.RemoveObserver(Notifications.CreditsButtonPressed, OnCreditsButton);
		this.RemoveObserver(Notifications.ExitButtonPressed, OnExitButton);
	}
}
