using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMachine : AbstractStateMachine
{
	public AbstractState runningState;

	protected override void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		ChangeStateIntervaled<AwakeTitle>(1);
	}
}
