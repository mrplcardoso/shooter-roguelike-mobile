using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TitleState : AbstractState
{
	public TitleMachine titleMachine
	{ get { return (TitleMachine)stateMachine; } }
}
