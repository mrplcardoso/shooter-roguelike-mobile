using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakeTitle : TitleState
{
	public override void OnEnter()
	{
		titleMachine.runningState = this;
		this.PostNotification(Notifications.TriggerTransitionScreen, new MessageArgs("open"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		titleMachine.ChangeStateIntervaled<MainTitle>();
	}
}
