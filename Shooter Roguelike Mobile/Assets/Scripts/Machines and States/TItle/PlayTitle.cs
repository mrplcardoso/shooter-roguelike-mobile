using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayTitle : TitleState
{
	public override void OnEnter()
	{
		titleMachine.runningState = this;
		this.PostNotification(Notifications.TriggerTransitionScreen, new MessageArgs("close"));
		this.AddObserver(OnTransitionCompleted, Notifications.TransitionCompleted);
	}

	public void OnTransitionCompleted(object sender, EventArgs e)
	{
		this.RemoveObserver(Notifications.TransitionCompleted, OnTransitionCompleted);
		SceneManager.LoadScene("PlayConfiguration");
	}
}
