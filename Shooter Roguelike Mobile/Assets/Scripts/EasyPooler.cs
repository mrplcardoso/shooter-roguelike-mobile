using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyPooler : MonoBehaviour
{
	public Objeto obj;
	public int numberOfClones = 10;

	public List<Objeto> clones;

	private void Start()
	{
		clones = new List<Objeto>();
		for(int i = 0; i < numberOfClones; ++i)
		{
			clones.Add(Instantiate(obj));
			obj.gameObject.SetActive(false);
		}

		StartCoroutine(InstantieateClone());
	}

	IEnumerator InstantieateClone()
	{
		while(true)
		{
			for(int i = 0; i < clones.Count; ++i)
			{
				if(!clones[i].gameObject.activeInHierarchy)
				{ 
					clones[i].gameObject.SetActive(true);
					clones[i].Activate();
					break; 
				}
			}
			yield return new WaitForSeconds(3);
		}
	}
}

public class Objeto : MonoBehaviour
{
	public void Activate()
	{
		StartCoroutine(DeActive());
	}
	IEnumerator DeActive()
	{
		yield return new WaitForSeconds(5);
	}
}