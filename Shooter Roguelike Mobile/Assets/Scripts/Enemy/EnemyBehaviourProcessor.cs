using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourProcessor : MonoBehaviour
{
	public EnemyHub enemyHub
	{ get; private set; }
	public EnemyObject enemyObject
	{ get { return enemyHub.enemyObject; } }
	public EnemyMove enemyMove
	{ get { return enemyHub.enemyMove; } }
	public EnemyLife enemyLife
	{ get { return enemyHub.enemyLife; } }

	//Objeto vazio na cena, que usa-se para
	//organizar os comportamentos (em forma de componentes)
	GameObject behaviourStorage;
	//Lista de comportamentos poss�veis
	[SerializeField]
	List<AbstractBehaviour> behaviourList;
	//Guarda o comportamento a ser executado
	AbstractBehaviour currentBehaviour;

	private void Awake()
	{
		enemyHub = GetComponent<EnemyHub>();
		behaviourStorage = GetComponentsInChildren<Transform>()[1].gameObject;
	}

	private void Start()
	{
		BuildBehaviourList();
	}

	void BuildBehaviourList()
	{
		//Retorna em forma de array todos os componentes adicionados no behaviourStorage
		//que herdam de AbstractBehaviour
		AbstractBehaviour[] ab = behaviourStorage.GetComponents<AbstractBehaviour>();
		behaviourList = new List<AbstractBehaviour>(ab.Length);
		//A partir do array, inicializa-se cada comportamento indicando qual IA o possui,
		//e adiciona o comportamento na lista.
		for (int i = 0; i < ab.Length; ++i)
		{
			ab[i].Initialize(this);
			behaviourList.Add(ab[i]);
		}
		//Ordena a lista de comportamentos a partir do valor da prioridade de cada comportamento
		//sendo que comportamentos de menor prioridade se encontram no in�cio da lista,
		//e os de maior, no fim da lista (ordena��o crescente)
		behaviourList.Sort((x, y) => x.behaviourPriority.CompareTo(y.behaviourPriority));
	}

	public void SelectBehaviour()
	{
		/*Varre a lista de comportamentos, verificando se cada um deles pode executar.
		 * Quando o m�todo CanAct() retorna verdadeiro, aquele comportamento pode executar.
		 * Nesse caso, verificamos se ele � um comportamento com maior prioridade, do que o
		 * comportamento previamente selecionado (currentBehaviour) - caso seja, sobreescreve-se
		 * o currentBehaviour com o novo comportamento de maior prioridade
		 */
		for(int i = 0; i < behaviourList.Count; ++i)
		{
			if(behaviourList[i].CanAct())
			{
				currentBehaviour = behaviourList[i];
			}
		}
	}

	public void ExecuteBehaviour()
	{
		if(currentBehaviour != null)
		{
			currentBehaviour.Act();
		}
	}

	private void Update()
	{
		SelectBehaviour();
		ExecuteBehaviour();
	}
}
