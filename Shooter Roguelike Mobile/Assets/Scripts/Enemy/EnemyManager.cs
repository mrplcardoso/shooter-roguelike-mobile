using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
	[SerializeField]
	DungeonLevel dungeon;
	[SerializeField]
	List<PoolableEnemy> prefabs;
	Dictionary<int, ObjectPooler<PoolableEnemy>> enemies;

	//Quantidade fixa de inimigos reservados por level;
	public const int fixedEnemyAmount = 5;
	public const int fixedMaxAmount = 10;
	int waveCount = 5;

	private void Start()
	{
		//Cria��o da lista de prefabs de forma ordenada
		PoolableEnemy[] p = Resources.LoadAll<PoolableEnemy>("Prefab/Enemies");
		prefabs = new List<PoolableEnemy>(p);
		//Ordena a lista do menor "level" para o maior
		prefabs.Sort((x, y) => x.level.CompareTo(y.level));

		//Pr�-aloca��o dos inimigos
		enemies = new Dictionary<int, ObjectPooler<PoolableEnemy>>();
		for (int i = 0; i < prefabs.Count; ++i)
		{
			ObjectPooler<PoolableEnemy> en = new ObjectPooler<PoolableEnemy>(prefabs[i],
				fixedEnemyAmount * PublicData.currentDungeonLevel,
				fixedMaxAmount * PublicData.currentDungeonLevel);
			enemies.Add(prefabs[i].level, en);
		}

		StartCoroutine(Spawn());
	}

	PoolableEnemy GetEnemy()
	{
		int maxValue = 3;
		int keyLevel = Mathf.Clamp((int)(RandomStream.NormalDistributionRange(1, maxValue)),
			1, enemies.Count);
		PoolableEnemy p = enemies[keyLevel].GetObject();
		return p;
	}

	IEnumerator Spawn()
	{
		yield return new WaitForSeconds(3f);
		while(waveCount > 0)
		{
			Vector2 position = dungeon.RandomRoomPosition();
			if (position.y != -5000)
			{
				float distance = (position - (Vector2)PublicData.playerReference.transform.position).sqrMagnitude;
				if (distance > 25f)
				{
					PoolableEnemy p = GetEnemy();
					p.transform.position = RandomStream.CirclePosition(position, 2);
					p.Activate(0);
					waveCount--;
				}
			}
			yield return new WaitForSeconds(5f);
		}
	}
}
