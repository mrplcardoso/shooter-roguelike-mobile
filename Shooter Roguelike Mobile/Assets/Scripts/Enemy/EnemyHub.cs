using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHub : MonoBehaviour
{
	public EnemyObject enemyObject
	{ get; private set; }
	public EnemyMove enemyMove
	{ get; private set; }
	public EnemyBehaviourProcessor enemyBehaviourProcessor
	{ get; private set; }
	public PoolableEnemy poolableEnemy
	{ get; private set; }
	public EnemyLife enemyLife
	{ get; private set; }
	public EnemyShoot enemyShoot
	{ get; private set; }

	private void Awake()
	{
		enemyObject = GetComponent<EnemyObject>();
		enemyMove = GetComponent<EnemyMove>();
		enemyBehaviourProcessor = GetComponent<EnemyBehaviourProcessor>();
		poolableEnemy = GetComponent<PoolableEnemy>();
		enemyLife = GetComponent<EnemyLife>();
		enemyShoot = GetComponent<EnemyShoot>();
	}
}
