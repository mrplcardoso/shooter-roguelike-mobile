using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLife : MonoBehaviour
{
	EnemyHub enemyHub;
	EnemyObject enemyObject
	{ get { return enemyHub.enemyObject; } }
	PoolableEnemy poolableEnemy
	{ get { return enemyHub.poolableEnemy; } }
	public Image lifeBar;//Definido pelo Inspector

	//Particula da explos�o a ser instanciada
	//quando o inimigo for destru�do
	[SerializeField]
	ParticleSystem explosion;

	float currentLife
	{
		get { return enemyObject.currentLife; }
		set { enemyObject.currentLife = Mathf.Clamp(value, 0f, enemyObject.maxLife); }
	}

	public float maxLife
	{ get { return enemyObject.maxLife; } }

	//2.5/maxlife = porcentagem que vale 2.5 em rela��o a maxlife
	public float lifePorcentage
	{ get { return (currentLife / enemyObject.maxLife); } }

	private void Awake()
	{
		enemyHub = GetComponent<EnemyHub>();
	}

	private void Start()
	{
		currentLife = enemyObject.maxLife;
		lifeBar.fillAmount = lifePorcentage;
	}

	public void AddLife(float recovery)
	{
		currentLife += recovery;
		lifeBar.fillAmount = lifePorcentage;
	}

	public void SubtractLife(float damage)
	{
		currentLife -= damage;
		lifeBar.fillAmount = lifePorcentage;
		if(currentLife == 0)
		{
			//Instancia part�cula de explos�o
			Instantiate(explosion, transform.position, Quaternion.identity);
			poolableEnemy.DeActivate(); 
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.CompareTag("Projectile"))//Verfica se esta colidindo com um projetil
		{
			ProjectileObject po = collision.gameObject.GetComponent<ProjectileObject>();
			//Verifica se a tag de quem disparou o projetil �
			//diferente da minha tag (no caso "minha tag" == tag do inimigo)
			if (!gameObject.CompareTag(po.ownerTag))
			{
				PoolableProjectile p = collision.gameObject.GetComponent<PoolableProjectile>();
				p.DeActivate();
				SubtractLife(0.5f);
			}
		}
	}
}
