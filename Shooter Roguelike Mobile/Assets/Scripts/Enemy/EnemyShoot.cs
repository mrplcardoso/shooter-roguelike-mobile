using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
  EnemyHub enemyHub;
	EnemyObject enemyObject
	{ get { return enemyHub.enemyObject; } }
  public PoolableProjectile prefab;
  ObjectPooler<PoolableProjectile> firePool;

  // Start is called before the first frame update
  void Awake()
  {
    enemyHub = GetComponent<EnemyHub>();
  }

	private void Start()
	{
    firePool = new ObjectPooler<PoolableProjectile>(prefab, 10, 50);
	}

	public void TriggerProjectile()
	{
		/*
		 Exemplo de busca de um alvo no campo de vis�o da IA:
		 A linha abaixo lan�a uma area de colis�o atraves do m�todo LookAround(), e 
		retorna a lista de objetos encontrados. Ao encontrar os objetos, marca como alvo
		a posi��o do primeiro objeto encontrado.
		Collider2D[] cols = enemyObject.LookAround();
		if(cols.Length == 0) { return; }
		Vector3 pos = enemyObject.LookAround()[0].transform.position;
		*/
		Vector3 pos = enemyObject.playerObject.gameObject.transform.position;
		pos.z = 0;
		PoolableProjectile p = firePool.GetObject();
		Vector3 v = transform.position;
		v.z = 0;
		p.transform.position = v;
		Vector2 dir = (pos - v).normalized;
		p.gameObject.layer = LayerMask.NameToLayer("EnemyLayer");
		p.GetComponent<ProjectileObject>().ownerTag =
			gameObject.tag;
		p.Activate(2f);
		p.GetComponent<ProjectileObject>().SetMove(
			dir, 4f);
	}
}
