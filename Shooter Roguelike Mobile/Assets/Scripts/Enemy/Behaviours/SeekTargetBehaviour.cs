﻿using System.Collections;
using UnityEngine;

public class SeekTargetBehaviour : AbstractBehaviour
{
	GameObject target
	{ get { return enemyOwner.enemyObject.playerObject.gameObject; } }

	public override void Act()
	{
		enemyOwner.enemyMove.direction = (target.transform.position -
			enemyOwner.transform.position).normalized;

		enemyOwner.enemyMove.Move();
	}

	public override bool CanAct()
	{
		/* Para que a IA reproduza o comportamento de seguir um alvo, são necessárias, algumas condições.
		 * A primeira é verificar se o alvo se encontra dentro do alcance da IA. No caso desse jogo, o alvo
		 * sempre será o jogador, portanto, o 'if' abaixo faz justamente essa verificação usando
		 * o jogador como alvo.
		 * A segunda condição para a IA seguir o alvo, é que ela ainda não tenha chegado a seu destino, pois,
		 * se já chegou ao destino, não ha mais sentido haver ação de seguir alvo.
		 */
		//Verifica se alvo esta em alcance
		if (enemyOwner.enemyObject.LookForTarget(target))
		{
			//Verifica se ainda não chegou no alvo
			if ((enemyOwner.transform.position - target.transform.position).sqrMagnitude > 4)
			{
				//if (enemyOwner.enemyMove.CanReachTarget(target))
				{ return true; }
			}
		}
		return false;
	}
}