﻿using System.Collections;
using UnityEngine;

public class RecoveryLifeBehaviour : AbstractBehaviour
{
	EnemyLife enemyLife
	{ get { return enemyOwner.enemyLife; } }
	float interval = 3f;
	float elapsedTime;
	public override void Act()
	{
		float recovery = enemyLife.maxLife * 0.5f;
		enemyLife.AddLife(recovery);
	}

	public override bool CanAct()
	{
		//A recuperação de vida só ocorrerá se a IA estiver
		//com 10% ou menos da vida total
		if(enemyLife.lifePorcentage <= 0.1f && Time.time > elapsedTime)
		{
			elapsedTime = Time.time + interval;
			return true; 
		}
		return false;
	}
}