﻿using System.Collections;
using UnityEngine;

public class ShootTarget : AbstractBehaviour
{
	EnemyShoot enemyShoot
	{ get { return enemyOwner.enemyHub.enemyShoot; } }
	PlayerObject target
	{ get { return enemyOwner.enemyObject.playerObject; } }

	float interval = 3f;
	float elapsedTime;

	public override void Act()
	{
		enemyShoot.TriggerProjectile();
	}

	public override bool CanAct()
	{
		//Condições para a execução do comportamento:
		//1- alvo precisa existir (diferente de nulo)
		//enemyOwner.enemyObject.target == null
		if (target == null)
		{ return false; }
		//2-alvo precisa estar no campo de visão da IA
		if(!enemyOwner.enemyObject.LookForTarget(target.gameObject))
		{ return false; }

		Vector3 pos = target.transform.position;
		pos.z = 0;
		Vector2 dir = (pos - transform.position).normalized;
		enemyOwner.enemyObject.body.rotation = (Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg) + 90f;

		//3-Tempo entre os disparos é menor que o tempo total decorrido do jogo
		if (Time.time < elapsedTime)
		{ return false; }

		elapsedTime = Time.time + interval;
		return true;
	}
}