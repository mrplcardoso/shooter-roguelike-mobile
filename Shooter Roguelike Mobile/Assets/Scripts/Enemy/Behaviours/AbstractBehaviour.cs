using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Classe abstrata que serve para implementar comportamentos de IA
public abstract class AbstractBehaviour : MonoBehaviour
{
	//Referencia que guarda a IA que executa esse comportamento
	public EnemyBehaviourProcessor enemyOwner
	{ get; private set; }
	//Guarda o valor de prioridade de execu��o desse comportamento
	[SerializeField]
	int priority;
	public int behaviourPriority
	{ get { return priority; } }
	public void Initialize(EnemyBehaviourProcessor owner)
	{
		enemyOwner = owner;
	}

	//M�todo destinado a verificar se o comportamento 
	//pode set executado
	public abstract bool CanAct();
	//M�todo que implementa a execu��o do comportamento
	public abstract void Act();
}
