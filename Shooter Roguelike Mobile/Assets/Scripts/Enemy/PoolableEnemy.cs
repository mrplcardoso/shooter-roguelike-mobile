using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableEnemy : MonoBehaviour, IPoolableObject
{
	public int level
	{ get { return enemyLevel; } }
	[SerializeField]
	int enemyLevel;//Definido pelo Inspector

	public int poolIndex 
	{ get { return id; } set { if (id < 0) { id = value; } } }
	[SerializeField]
	int id = -1;

	public bool activeInScene
	{ get { return gameObject.activeInHierarchy; } }

	float timer;

	public float leftDuration
	{ get { return timer; } }

	public bool stopTimer 
	{ get { return (timer <= 0) ? true : false; }
		set { if (value) { timer = 0; } } }

	public void Activate(float duration)
	{
		gameObject.SetActive(true);
		timer = duration;
		if (duration > 0)
		{ StartCoroutine(LifeTime()); }
	}

	public void DeActivate()
	{
		gameObject.SetActive(false);
	}

	IEnumerator LifeTime()
	{
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		DeActivate();
	}
}
