using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyObject : MonoBehaviour
{
	public PlayerObject playerObject 
	{ get { return PublicData.playerReference; } }

	public Rigidbody2D body { get; private set; }
	public BoxCollider2D collider { get; private set; }

	public LayerMask mask;
	public readonly float visionField = 5f;

	public readonly float maxLife = 5f;
	public float currentLife;

	private void Awake()
	{
		body = GetComponent<Rigidbody2D>();
		collider = GetComponent<BoxCollider2D>();
	}

	public Collider2D[] LookAround()
	{
		return Physics2D.OverlapCircleAll(transform.position, visionField, mask);
	}

	public bool LookForTarget(GameObject searchObject)
	{
		//Se o n�mero de colliders for maior que zero, significa que 
		//a IA possui ao menos um objeto dentro do seu campo de vis�o
		Collider2D[] cols = LookAround();
		if (cols.Length > 0)
		{
			for (int i = 0; i < cols.Length; ++i)
			{
				if (cols[i].gameObject == searchObject)
				{ return true; }
			}
		}
		return false;
	}

	private void Update()
	{
		
	}
}

/*A��es:
 * - Perseguir player
 * - Torreta
 * - Recuperar HP
 */