using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
	EnemyHub enemyHub;
	EnemyObject enemyObject
	{ get { return enemyHub.enemyObject; } }
	Rigidbody2D body
	{ get { return enemyObject.body; } }
	BoxCollider2D collider
	{ get { return enemyObject.collider; } }
	PlayerObject playerObject
	{ get { return enemyObject.playerObject; } }

	public Vector2 direction;
	float speed;

	private void Awake()
	{
		enemyHub = GetComponent<EnemyHub>();
	}

	private void Start()
	{
		//playerObject.AddPlayerObserver(OnPlayerAct);
		speed = 2f;
		//StartCoroutine(SetWanderDirection());
	}
	/*
	void OnPlayerAct(EventArgs e)
	{
		//A IA s� mover� em dire��o ao jogador, se o jogador estiver 
		//no seu campo de vis�o
		if(enemyObject.LookForTarget(playerObject.gameObject))
		{
			if (CanReachTarget(playerObject.gameObject))
			{ direction = playerObject.transform.position - transform.position; }
		}
		//Caso o jogador esteja fora do campo de vis�o da IA, no presente momento
		//do c�digo, o calculo de dire��o do algoritmo vagar � feito concomitante
		//com os demais comportamentos da IA, e, portanto, a IA se mover�
		//na dire��o sorteada
		Move();
	}*/

	public void Move()
	{
		body.MovePosition(body.position + direction.normalized * speed * Time.fixedDeltaTime);
		enemyHub.enemyObject.body.rotation = (Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg) + 90f;
	}

	public bool CanReachTarget(GameObject target)
	{
		/* Esse m�todo verifica se existe algum obst�culo que impe�a a IA de se mover em dire��o ao alvo
		 * Ele lan�a um BoxCast no cen�rio em dire��o ao alvo e verifica o primeiro objeto com que o BoxCast
		 * colide. Caso seja o alvo, retorna verdadeiro pois n�o existe obstaculo entre eles, caso n�o seja
		 * retorna falso pois existe algo intranspon�vel entre eles.
		*/
		Vector2 dir = target.transform.position - transform.position;
		RaycastHit2D hit = Physics2D.BoxCast(transform.position, collider.size,
			0, dir.normalized, dir.magnitude, enemyObject.mask);
		if (hit.collider != null)
		{
			if (hit.collider.gameObject == target)
			{ return true; }
		}
		return false;
	}

	IEnumerator SetWanderDirection()
	{
		float timer = 0;
		float startTime = 5f;
		while (true)
		{
			while (timer > 0)
			{
				timer -= Time.deltaTime;
				yield return null;
			}
			//Sortear uma dire��o
			//"insideUnitCircle" retorna uma posi��o aleat�ria dentro de um c�rculo
			//ao normalizar essa posi��o, conceitualmente ela se torna uma dire��o
			//direction = UnityEngine.Random.insideUnitCircle.normalized;
			direction = RandomStream.Direction2D();
			timer = startTime;
			yield return null;
		}
	}
}
