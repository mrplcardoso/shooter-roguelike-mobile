using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTest : MonoBehaviour
{
  public GameObject prefab;
  public RandomStream rng;
	public int seed;

	private void Start()
	{
		rng = new RandomStream(seed);
		StartCoroutine(Instance());
	}

	IEnumerator Instance()
	{
		while (true)
		{
			GameObject p = Instantiate(prefab);
			p.transform.position =
				rng.normalDistributionRange2D(new Vector2(-20, -20), new Vector2(20, 20));
			yield return new WaitForSeconds(0.05f);
		}
	}
}
