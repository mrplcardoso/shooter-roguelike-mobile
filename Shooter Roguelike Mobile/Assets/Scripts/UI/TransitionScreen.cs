using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionScreen : MonoBehaviour
{
	[SerializeField]
	Vector3 inScene, outScene;
	[SerializeField]
	float speed;
	RectTransform rectTransform;

	private void Awake()
	{
		this.AddObserver(RunTransition, Notifications.TriggerTransitionScreen);
		rectTransform = GetComponent<RectTransform>();
	}

	public void RunTransition(object sender, EventArgs e)
	{
		string transition = (string)((MessageArgs)e).message;
		if (transition == "close")
		{
			StartCoroutine(Transition(outScene, inScene, speed));
		}
		else if (transition == "open")
		{
			StartCoroutine(Transition(inScene, outScene, speed));
		}
	}

	IEnumerator Transition(Vector3 start, Vector3 end, float speed)
	{
		float t = 0;
		while(t < 1.01f)
		{
			rectTransform.localPosition = EasingEquations.Linear(start, end, t);
			t += speed * Time.deltaTime;
			yield return null;
		}
		rectTransform.localPosition = end;
		yield return null;
		this.PostNotification(Notifications.TransitionCompleted);
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.TriggerTransitionScreen, RunTransition);
	}
}
