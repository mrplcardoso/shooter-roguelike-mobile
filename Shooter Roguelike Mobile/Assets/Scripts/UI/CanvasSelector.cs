using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSelector : MonoBehaviour
{
	[SerializeField]
	string enableEvent, disableEvent;
	[SerializeField]
	Canvas canvas;

	private void Awake()
	{
		canvas = GetComponentInChildren<Canvas>(true);
		this.AddObserver(EnableCanvas, enableEvent);
		this.AddObserver(DisableCanvas, disableEvent);
	}

	public void EnableCanvas(object sender, EventArgs e)
	{
		canvas.gameObject.SetActive(true);
	}

	public void DisableCanvas(object sender, EventArgs e)
	{
		canvas.gameObject.SetActive(false);
	}

	private void OnDestroy()
	{
		this.RemoveObserver(enableEvent, EnableCanvas);
		this.RemoveObserver(disableEvent, DisableCanvas);
	}
}
