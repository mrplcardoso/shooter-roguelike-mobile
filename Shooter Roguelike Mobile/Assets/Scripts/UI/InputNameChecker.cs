using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputNameChecker : MonoBehaviour
{
	TMP_InputField field;

	private void Awake()
	{
		field = GetComponent<TMP_InputField>();
		this.AddObserver(OnButtonPressed, Notifications.StartButtonPressed);
	}

	public void OnButtonPressed(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(field.text))
		{
			PublicData.currentPlayerName = field.text;
			this.PostNotification(Notifications.ValidNamePlay, new MessageArgs(true));
		}
		else
		{ this.PostNotification(Notifications.ValidSeedPlay, new MessageArgs(false)); }
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.StartButtonPressed, OnButtonPressed);
	}
}
