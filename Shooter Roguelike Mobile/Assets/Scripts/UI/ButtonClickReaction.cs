﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickReaction : MonoBehaviour
{
	[SerializeField]
	string observeNotification, postNotification;
	Button button;

	private void Awake()
	{
		button = GetComponent<Button>();
	}

	private void Start()
	{
		button.onClick.AddListener(ClickEvent);
	}

	public void ClickEvent()
	{
		this.PostNotification(postNotification);
	}
}