using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InputSeedChecker : MonoBehaviour
{
	TMP_InputField field;

	private void Awake()
	{
		field = GetComponent<TMP_InputField>();
		this.AddObserver(OnButtonPressed, Notifications.StartButtonPressed);
		this.AddObserver(IsRandomSeed, Notifications.IsRandomSeedInput); 
	}

	public void OnButtonPressed(object sender, EventArgs e)
	{
		if (!string.IsNullOrEmpty(field.text))
		{
			PublicData.currentSeed = int.Parse(field.text);
			this.PostNotification(Notifications.ValidSeedPlay, new MessageArgs(true));
		}
		else
		{ this.PostNotification(Notifications.ValidSeedPlay, new MessageArgs(false)); }
	}

	public void IsRandomSeed(object sender, EventArgs e)
	{
		bool b = (bool)((MessageArgs)e).message;

		if (b)
		{
			field.text = "-1";
			field.enabled = false;
		}
		else
		{
			field.enabled = true;
		}
	}

	private void OnDestroy()
	{
		this.RemoveObserver(Notifications.StartButtonPressed, OnButtonPressed);
		this.RemoveObserver(Notifications.IsRandomSeedInput, IsRandomSeed);
	}
}
