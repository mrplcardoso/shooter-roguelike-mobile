using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleChecker : MonoBehaviour
{
	Toggle toggle;

	private void Awake()
	{
		toggle = GetComponent<Toggle>();
		toggle.onValueChanged.AddListener(OnChange);
	}

	public void OnChange(bool b)
	{
		this.PostNotification(Notifications.IsRandomSeedInput, new MessageArgs(b));
	}
}
