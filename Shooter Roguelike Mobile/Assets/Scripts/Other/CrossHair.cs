using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHair : MonoBehaviour
{
	SpriteRenderer renderer;
	public Vector3 overTargetPosition
	{ set { transform.position = 
		new Vector3(value.x, value.y, transform.position.z);	} }

	private void Start()
	{
		renderer = GetComponent<SpriteRenderer>();
		renderer.enabled = false;
	}

	public void SetOverTarget(Vector3 position)
	{
		overTargetPosition = position;
		renderer.enabled = true;
		renderer.color = Color.white;
		StopAllCoroutines();
		StartCoroutine(Fade(1));
	}

	IEnumerator Fade(float speed)
	{
		float t = 0;
		while(t < 1.01f)
		{
			renderer.color = Color.Lerp(Color.white, Color.clear, t);
			t += speed * Time.deltaTime;
			yield return null;
		}
		renderer.enabled = false;
	}
}
