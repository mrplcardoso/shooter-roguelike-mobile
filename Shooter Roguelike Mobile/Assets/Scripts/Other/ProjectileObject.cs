using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileObject : MonoBehaviour
{
	public string ownerTag;
	float speed;

	public void SetMove(Vector2 direction, float speed)
	{
		this.speed = speed;
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg -90f;
		transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
	}

	private void Update()
	{
		Move();
	}

	void Move()
	{
		transform.position += transform.up * speed * Time.deltaTime;
	}
}
