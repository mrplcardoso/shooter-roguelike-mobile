using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PublicData
{
	public static PlayerObject playerReference;
	public static RandomStream mapRandom = new RandomStream();
	public static string currentPlayerName = "no name";
	public static int currentSeed = -1;
	public static int numberOfMainRooms = 3;
	public static int numberOfSideRooms = 1;
	public static int depthOfSidePaths = 1;
	public static int currentDungeonLevel = 5;
}
