using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Notifications
{
	public const string TriggerTransitionScreen = "TriggerTransitionScreen";
	public const string TransitionCompleted = "TransitionCompleted";

	/*Title Screen Events*/
	#region Title Screen
	public const string PlayButtonPressed = "PlayButtonPressed";
	public const string RankingButtonPressed = "RankingButtonPressed";
	public const string CreditsButtonPressed = "CreditsButtonPressed";
	public const string ExitButtonPressed = "ExitButtonPressed";
	public const string ExitCreditsButtonPressed = "ExitCreditsButtonPressed";
	public const string ExitRankingButtonPressed = "ExitRankingButtonPressed";

	public const string EnterMainState = "EnterMainState";
	public const string ExitMainState = "ExitMainState";

	public const string EnterPlayState = "EnterPlayState";
	public const string ExitPlayState = "ExitPlayState";

	public const string EnterRankingState = "EnterRankingState";
	public const string ExitRankingState = "ExitRankingState";

	public const string EnterCreditsState = "EnterCreditsState";
	public const string ExitCreditsState = "ExitCreditsState";

	public const string EnterExitState = "EnterExitState";
	public const string ExitExitState = "ExitExitState";
	#endregion

	/*Play Config. Events*/
	#region Play Config.
	public const string StartButtonPressed = "StartButtonPressed";
	public const string ValidNamePlay = "ValidNamePlay";
	public const string ValidSeedPlay = "ValidSeedPlay";
	public const string IsRandomSeedInput = "IsRandomSeedInput";
	public const string BackButtonPressed = "BackButtonPressed";

	public const string EnterMainPlayState = "EnterMainPlayState";
	public const string ExitMainPlayState = "ExitMainPlayState";
	#endregion

	/* Game Events*/
	#region Game
	public const string RoomGenerationCompleted = "RoomGenerationCompleted";
	#endregion
}